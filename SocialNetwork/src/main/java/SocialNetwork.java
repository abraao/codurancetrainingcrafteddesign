public class SocialNetwork {

	private final SocialNetworkConsole console;
	private final MessageRepository messageRepository;

	public SocialNetwork(SocialNetworkConsole console,
						 MessageRepository messageRepository) {
		this.console = console;
		this.messageRepository = messageRepository;
	}

	public void run() {
		while(true) {
			String command = console.read();

			if(command.equals("exit")) {
				return;
			}

			String[] commandParts = command.split(" -> ");

			String userName = commandParts[0];
			String message = commandParts[1];

			messageRepository.addMessage(userName, message);
		}
	}
}
