import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageRepository {

	private Map<String, List<String>> allMessages = new HashMap<String, List<String>>();
	private final ArrayList<String> EMPTY_MESSAGES = new ArrayList<String>(0);

	public void addMessage(String user, String message) {
		List<String> userMessages;
		if(!allMessages.containsKey(user)) {
			userMessages = new ArrayList<String>();
		}
		else {
			userMessages = allMessages.get(user);
		}

		userMessages.add(message);
		allMessages.put(user, userMessages);
	}

	public List<String> getMessages(String user) {
		if(!allMessages.containsKey(user)) {
			return EMPTY_MESSAGES;
		}

		return allMessages.get(user);
	}
}
