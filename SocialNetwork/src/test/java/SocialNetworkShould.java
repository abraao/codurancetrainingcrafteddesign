import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SocialNetworkShould {

	@Mock
	private SocialNetworkConsole console;

	@Mock
	private MessageRepository messageRepository;

	@Test
	public void
	run_until_it_receives_exit_command() {
		when(console.read()).thenReturn("exit");

		SocialNetwork socialNetwork = new SocialNetwork(console, messageRepository);
		socialNetwork.run();

		verify(console).read();
	}

	@Test
	public void
	post_a_message() {
		when(console.read())
				.thenReturn("Alice -> I love the weather today")
				.thenReturn("exit");

		SocialNetwork socialNetwork = new SocialNetwork(console, messageRepository);

		socialNetwork.run();

		verify(messageRepository).addMessage("Alice", "I love the weather today");
	}
}
