import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class MessageRepositoryShould {

	@Test
	public void
	return_empty_list_for_user_that_doesnt_exist() {
		MessageRepository messageRepository = new MessageRepository();

		List<String> userMessages = messageRepository.getMessages("Alice");

		assertEquals(0, userMessages.size());
	}

	@Test
	public void
	return_message_added_for_new_user() {
		MessageRepository messageRepository = new MessageRepository();

		final String user = "Alice";
		final String message = "foobar";

		messageRepository.addMessage(user, message);
		List<String> userMessages = messageRepository.getMessages(user);

		assertEquals(1, userMessages.size());
		assertEquals(message, userMessages.get(0));
	}

	@Test
	public void
	return_message_added_for_existing_user() {
		MessageRepository messageRepository = new MessageRepository();

		final String user = "Alice";
		final String message = "foobar";
		final String message2 = "example message";

		messageRepository.addMessage(user, message);
		messageRepository.addMessage(user, message2);

		List<String> userMessages = messageRepository.getMessages(user);

		assertEquals(2, userMessages.size());
		assertEquals(message, userMessages.get(0));
		assertEquals(message2, userMessages.get(1));
	}
}
