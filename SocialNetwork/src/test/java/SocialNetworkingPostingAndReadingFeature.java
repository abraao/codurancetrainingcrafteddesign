import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class SocialNetworkingPostingAndReadingFeature {

	@Mock
	private SocialNetworkConsole console;

	@Mock
	private MessageRepository messageRepository;

	@Test
	public void
	should_read_a_message_already_a_user_posted() {
		SocialNetwork socialNetwork = new SocialNetwork(console, messageRepository);

		socialNetwork.run();

		verify(console).read().equals("Alice -> I love the weather today");
		verify(console).read().equals("Alice");
		verify(console).print("I love the weather today (5 minutes ago)");
		verify(console).read().equals("exit");
	}
}
