# Codurance Training 2017 - Crafted Design

sandro@codurance.com, Twitter: @sandromancuso, @codurance


## Agenda

Day 1

* TDD & Naming
* Emergent design via TDD
* SOLID & BAP
* Outside-In TDD & Design

Day 2

* Interaction Driven Design (IDD)
* IDD Modeling
* Identifying services through business rules
* Deriving architecture through Impact Mapping


## Prep

Naming convention:

* `*Should`
* Focus onthe behavior, not on the public method.
* Start *from the assertion* and then climb up.


## Emergent Design via TDD

Difference between Classicist and Outside-In TDD

* Classicist doesn't prescribe direction. Can do Outside-In TDD as well.
* We'll do Classicist outside-in. The public method will get moore omplicated as we add more tests.
 * In Classicist we create a mess then we move the code around.
 * Go to green as soon as possible. Don't focus on clean code; make it work first. Then stop and refactor.
* The problem with design in Classicist is that you need to develop a gut feeling, because if you extract too early you may start to overgineer.
 * It gets complicated when we extract methods and classes, because those impose a constraint/structure.
* When do we use Classicist? When you know what the inputs and the outputs should be, but you don't know what should be in the middle.
 * Advantage: very rarely overengineer.
 * Disadvantage: very slow method.
 * Refactoring phases are always larger due to a larger unit (i.e. multiple classes).
* An unit test is not necessarily about a clas or a method, because an unit could be a class, a method, a group of classes.
* When a class has a life on its own then we can add unit test.

### Exercise - Classicist TDD - Mars Rover Kata

* Let the code evolve without mocks. Don't use mocks.

#### Description

Develop an API that moves a rover around on a grid.
 
Rules:
 
- You are given the initial starting point (0,0,N) of a rover.
- 0,0 are X,Y co-ordinates on a grid of (10,10).
- N is the direction it is facing (i.e. N,S,E,W).
- L and R allow the rover to rotate left and right.
- M allows the rover to move one point in the current direction.
- The rover receives a char array of commands e.g. RMMLM and returns the
    finishing point after the moves e.g. 2,1,N
- The rover wraps around if it reaches the end of the grid.
- The grid may have obstacles. If a given sequence of commands encounters an
    obstacle, the rover moves up to the last possible point and reports the
    obstacle e.g. O,2,2,N


#### Comments during the exercise

The tests should always describe the general behavior of a class, but *the tests should not be named after the examples*. i.e. should be facing east after turning right. Rules are generic, examples are specific. e.g. mars rover should move right, mars rover should move forward.

* If using paraemterized tests don't mix concepts.
* In a test a single assertion means asserting a single concept, not a single assert statement.
* Finish testing one concept at a time.

Most of us have passed the point of refactoring. When to do it? For example: after doing rotation, we have a mess but it's only rotation, so we can do a refactor.


#### Working through Sandro's solution

* Starting with turn right once. Returns static value.
 * Added runner `JUnitParamsRunner` and the parameters.
 * Doing `RR` command. Iterating over char array of commands. Created field for direction.
 * Adding more test cases for turning right.
 * He chose this test first because it was the simplest test. Starting with move is not great because moving depends on direction.
 * Added if statement to check for R command and extracted `turnRight()` command.
* [Object Calistenics](https://www.bennadel.com/resources/uploads/2012/ObjectCalisthenics.pdf): 9 rules that if you follow them you should end up with better code.
* Handling turning left.
 * If statement for L command.
 * Creating method turning left. The structure is similar to turning left.
 * Adding more cases for L commands.
* Question: should we use code smells to trigger refactoring?
 * When we are done with the class, we can inspect the code smells and address then. Before that, if I'm in a feature, we need to determine the shape of the class.
* Exploring left/right and positions with enum. e.g. `NORTH("N", "W", "E")`
 * Added constructor to the enum and a move method.
 * Before we start going crazy with clean code, ask if the behavior belongs to the class or whether we need to create a new home for it. These are the baby steps that he would use in the Classicist approach.
* Now we can start thinking about moving. Starting with moving one position.
 * Created field for y. Created method `move()` that increments y by 1 and test `move_north()`
* Created test case `move_south()`
 * New test case: `MRRM`. Added if statements check if direction is north or south and incrementing / decrementing y.
 * New test case: `MMMMMMMMRRMM`
* Instead of correctness in the design, we should consider how much information we want to expose to the external world. Is this collaboration that I'm passing somethig that exists on its own?
* When we're exposing things to the world, we start needing validation for things that we didn't need before. e.g. valid position.
 * The class using the `MarsRover` won't know the size of the grid.
* Sandro's final solution: he created a `Grid` class that specifies max height and max length, as well as a list of obstacles (that are coordinates).
 * The grid has a `nextCellPosition` that takes a coordinate and a direction.
 * What he didn't like with this Classicist approach is that most of the tests are in the Rover class. Some of the tests were only testing things that are in the `Grid`.
 * He ended up with a test in the `Grid` class that is only related to obstacles.
 * Some of the tests in the `MarsRover` he'd move to the `Grid` class.
 * The `Grid` is an association, it exists without the `MarsRover`. For that reason alone the `Grid` should have its own tests.
 * What is the likelihood of the `Grid` evolving separately from the `MarsRover`? This can dictate where to place the tests. Is the `Grid` using in different contexts? As soon as we need it in a different context then we can move the tests.


## SOLID


### SRP - Single Responsibility Principle

How do I know a method has more than one responsbility?

* A method that has more than one reason to change.


### OCP - Open Closed Principle

How does it work in practice?

* Initially it was about inheritance, it was mixed up with the extends keyword. This was a bad mistake.
* This principle is on a different level than the other principles. This is more of an architectural pattern.


### LSP - Liskov Substitution Principle

Skipped


### ISP - Interface Segregation Principle

When do we use it?

What is it? What do we do?

* It was originally used to solve a C++ problem. They had these god classes accessed by multiple clients, and a change required recompilation of a lot of code. By creating client specific interfaces they solved the compilation problem.
* This is a good pattern to break up god classes.


### Dependency Injection

Self-explanatory


## BAP - Balanced Abstraction Principle

*Software constructs (classes, modules, functions, etc) grouped by a higher-level construct should be on the same level of abstraction.*

That means:

* All instructions inside a method should be at the same level of abstraction.
* All public methods inside a class should be at the same level of abstraction.
* All public classes inside a module (package/namespace sould be at the same level of abstration.
* All sibling modules inside a parent module should be at the same leel of abstraction.
* All components, services, etc.

Examples:

* `PaymentService` class with payment related methods and a `toJson` method.

LCOM4 - Lack of Cochesion of Methods

* Optimal number is 1.
* As soon as you have a metric bigger than 1, you have an opportunity to split the class. We break this rule, but this is a good guideline.
 * When do we break LCOM4? e.g. Controllers, Repositories

## Exercise 2 - Outside-In TDD

Implement a console-based social entworking application (siilar to Twitter) satisfying the scenarios below.

* Reading
* Posting
* Following & Wall

* Assume all inputs are correct and no validation are needed.
* Every single thing should be test driven.

### Description

```
Implement a console-based social networking application (similar to Twitter)
satisfying the scenarios below.
 
Features
 
Posting: Alice can publish messages to a personal timeline
 
> Alice -> I love the weather today
> Bob -> Damn! We lost!
> Bob -> Good game though.
 
Reading: I can view Alice and Bob’s timelines
 
> Alice
I love the weather today (5 minutes ago)
> Bob
Good game though. (1 minute ago)
Damn! We lost! (2 minutes ago)
 
Following & Wall: Charlie can subscribe to Alice’s and Bob’s timelines,
and view an aggregated list of all subscriptions
 
> Charlie -> I'm in New York today! Anyone want to have a coffee?
> Charlie follows Alice
> Charlie wall
Charlie - I'm in New York today! Anyone want to have a coffee? (2 seconds ago)
Alice - I love the weather today (5 minutes ago)
 
> Charlie follows Bob
> Charlie wall
Charlie - I'm in New York today! Anyone wants to have a coffee? (15 seconds ago)
Bob - Good game though. (1 minute ago)
Bob - Damn! We lost! (2 minutes ago)
Alice - I love the weather today (5 minutes ago)
 
Details
 
The application must use the console for input and output.
Users submit commands to the application. There are five commands:
 
posting: <user name> -> <message>
reading: <user name>
following: <user name> follows <another user>
wall: <user name> wall
exit: exit
 
“posting”, “reading”, “following” and “wall” are not part of the commands;
commands always start with the user’s name.
 
Don't worry about handling any exceptions or invalid commands. Assume that
the user will always type the correct commands. Just focus on the sunny day
scenarios.Don’t bother making it work over a network or across processes.
It can all be done in memory, assuming that users will all use the same terminal.
Non-existing users should be created as they post their first message.
Application should not start with a pre-defined list of users.
```


### Clarifications

* The application must display the prompt when waiting for input.
* The application should say bye when exiting.
* How can I make the 5 things that this class needs to do be OCP compliant?
 * I have two commands and two queries.


### Code

```java
package console_twitter.com.codurance.crafted_design.unit;
 
import com.codurance.crafted_design.command.CommandExecutor;
import com.codurance.crafted_design.Twitter;
import com.codurance.crafted_design.view.Console;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
 
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
 
@RunWith(MockitoJUnitRunner.class)
public class TwitterShould {
 
  private static final String POST_COMMAND = "Alice -> Hello";
  private static final String READ_COMMAND = "Alice";
  private static final String EXIT = "exit";
  @Mock private Console console;
  @Mock private CommandExecutor commandExecutor;
 
  private Twitter twitter;
 
  @Before
  public void initialise() {
      twitter = new Twitter(console, commandExecutor);
  }
 
  @Test public void
  terminate_when_an_exit_command_is_received() {
      given(console.readline()).willReturn("a command", "exit");
 
      twitter.start();
 
      verify(console).write("bye!");
  }
 
  @Test public void
  execute_the_user_command() {
      given(console.readline()).willReturn(POST_COMMAND, READ_COMMAND, EXIT);
 
      twitter.start();
 
      InOrder inOrder = Mockito.inOrder(commandExecutor, console);
      inOrder.verify(commandExecutor).execute(POST_COMMAND);
      inOrder.verify(commandExecutor).execute(READ_COMMAND);
      verify(console).write("bye!");
  }
 
}
 
 
package com.codurance.crafted_design;
 
import com.codurance.crafted_design.command.CommandExecutor;
import com.codurance.crafted_design.command.CommandFactory;
import com.codurance.crafted_design.core.domain.Clock;
import com.codurance.crafted_design.core.domain.UserRepository;
import com.codurance.crafted_design.core.infrastructure.SystemClock;
import com.codurance.crafted_design.core.use_cases.AddPostUseCase;
import com.codurance.crafted_design.core.use_cases.FollowUseCase;
import com.codurance.crafted_design.core.use_cases.ReadPostsUseCase;
import com.codurance.crafted_design.core.use_cases.WallUseCase;
import com.codurance.crafted_design.view.Console;
 
public class Twitter {
 
  private static final String EXIT = "exit";
  private final Console console;
  private final CommandExecutor commandExecutor;
 
  public Twitter(Console console, CommandExecutor commandExecutor) {
      this.console = console;
      this.commandExecutor = commandExecutor;
  }
 
  public void start() {
      String userCommand = console.readline();
      while(!EXIT.equals(userCommand)) {
          commandExecutor.execute(userCommand);
          userCommand = console.readline();
      }
      console.write("bye!");
  }
 
  public static void main(String[] args) {
      twitterConsole().start();
  }
 
  private static Twitter twitterConsole() {
      // Create TwitterApp
  }
 
}
```


### Queries vs Commands

|             | Queries                                                     | Commands                                                       |
|-------------|-------------------------------------------------------------|----------------------------------------------------------------|
| Twitter     | [Image](htts://goo.gl/44lCnK), [File](htts://goo.gl/LKjxle) | [Image](htts://goo.gl/PQK9vx), [File](htts://goo.gl/Yqrt0d) |
| PostCommand | [Image](htts://goo.gl/3S5R7z), [File](htts://goo.gl/Ukm2Jr) | [Image](htts://goo.gl/O2N03x), [File](htts://goo.gl/v7OsHW) |
| ReadCommand | [Image](htts://goo.gl/IJbBSv), [File](htts://goo.gl/bT77Df) | [Image](htts://goo.gl/XzXqFQ), [File](htts://goo.gl/5JUp4U) |

<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==" alt="Red dot" />



## Day 1 Retrospective

* If a private method grows and we feel the need to unit test separately, that method is too important to be a detail of a class and deserves its own home in the domain.
