import craftsmanship.MarsRover;
import craftsmanship.MarsRoverPosition;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MarsRoverShouldTests {


    @Test public void
    move_one_position_forward() {

        MarsRover marsRover = new MarsRover();

        assertEquals("0,1,N", marsRover.move("M"));
    }

    @Test public void
    move_forward_and_turn_right() {

        MarsRover marsRover = new MarsRover();

        assertEquals("0,1,E", marsRover.move("MR"));
    }

    @Test public void
    be_facing_west_when_moving_left() {

        MarsRover marsRover = new MarsRover();

        assertEquals("0,0,W", marsRover.move("L"));
    }

    @Test public void
    move_forward_two_positions() {

        MarsRover marsRover = new MarsRover();

        assertEquals("0,2,N", marsRover.move("MM"));
    }

    
}
