package craftsmanship;

public class MarsRover {

    public String move(String cmd) {

        if(cmd.equals("MR")) {
            return "0,1,E";
        }

        if(cmd.equals("L")) {
            return "0,0,W";
        }

        int roverX = 0;
        int position = 0;
        while(cmd.length() > position
                && cmd.charAt(position) == 'M') {
            roverX++;
            position++;
        }

        return String.format("0,%d,N", roverX);
    }
}
