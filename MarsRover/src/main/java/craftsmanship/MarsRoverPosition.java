package craftsmanship;

/**
 * Created by davidespart on 06/06/2017.
 */
public class MarsRoverPosition {
    private int x;
    private int y;
    private String d;

    public MarsRoverPosition(int x, int y, String d) {

        this.x = x;
        this.y = y;
        this.d = d;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getD() {
        return d;
    }


}
